FROM registry.gitlab.com/hackathon-bbva/plumbr:latest

LABEL maintainer "Fabian Santander <hdmsantander@gmail.com>"

ENV DATABASE_HOST localhost
ENV DATABASE_USER datastream
ENV DATABASE_PASSWORD datastream
ENV DATABASE_NAME datastream
ENV DATABASE_PORT 5432

ENV FLASK_APP api_python.py

RUN R -e "version"

WORKDIR /srv/backend

COPY ./backend/ /srv/backend

EXPOSE 8051 8052

RUN ["chmod", "+x", "/srv/backend/script.sh"]

CMD ["/srv/backend/script.sh"]
