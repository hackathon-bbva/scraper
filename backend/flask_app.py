from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from bs4 import BeautifulSoup
from flask import Flask
import csv

app = Flask(__name__)

@app.route('/', methods=['GET'])
def health():
    return ("Healthy")

@app.route('/cnvb/040', methods=['GET'])
def scrape040():
    driver = webdriver.Remote(command_executor='http://selenium.ds:4444/wd/hub',desired_capabilities=DesiredCapabilities.CHROME)
    driver.implicitly_wait(15)
    driver.get("https://picomponentebi.cnbv.gob.mx/ReportViwer/ReportService?sector=40&tema=2&subTema=3&tipoInformacion=0&subTipoInformacion=0&idReporte=040_11q_BIPER0&idPortafolio=0&idTipoReporteBI=1")
    driver.set_window_size(960, 1043)
    
    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")
        
    driver.implicitly_wait(3)
    driver.find_element(By.ID, "btn_OcultaFiltros").click()
    
    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")
        
    driver.find_element(By.CSS_SELECTOR, ".radio:nth-child(1) > div:nth-child(4) > label").click()
    
    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")
    driver.find_element(By.ID, "chk_202007").click()
    driver.find_element(By.ID, "chk_202006").click()
    driver.find_element(By.ID, "chk_202005").click()
    driver.find_element(By.ID, "chk_202004").click()
    
    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")
    
    driver.find_element(By.LINK_TEXT, "Institución").click()
    driver.find_element(By.ID, "chk______Actinver").click()
    driver.find_element(By.ID, "chk______Afirme").click()
    driver.find_element(By.ID, "chk______Autofin").click()
    driver.find_element(By.ID, "chk______Banamex").click()
    
    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")
        
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(6) > td:nth-child(1) > label").click()
    driver.find_element(By.ID, "chk______Banco_Ahorro_Famsa").click()
    driver.find_element(By.ID, "chk______Banco_Azteca").click()
    driver.find_element(By.ID, "chk______BanCoppel").click()
    driver.find_element(By.ID, "chk______Bank_of_America").click()
    driver.find_element(By.ID, "chk______Bank_of_China").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(4) > td:nth-child(2) > label").click()
    driver.find_element(By.ID, "chk______Banorte").click()
    driver.find_element(By.ID, "chk______Banregio").click()
    driver.find_element(By.ID, "chk______BBVA_Bancomer").click()
    element = driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D > .checkbox")
    actions = ActionChains(driver)
    actions.move_to_element(element).click_and_hold().perform()
    element = driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D > .checkbox")
    actions = ActionChains(driver)
    actions.move_to_element(element).perform()
    element = driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D > .checkbox")
    actions = ActionChains(driver)
    actions.move_to_element(element).release().perform()
    
    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")
        
    driver.find_element(By.ID, "chk______HSBC").click()
    driver.find_element(By.ID, "chk______ICBC").click()
    driver.find_element(By.ID, "chk______Inbursa").click()
    driver.find_element(By.ID, "chk______Inmobiliario_Mexicano").click()
    driver.find_element(By.ID, "chk______Intercam_Banco").click()
    driver.find_element(By.ID, "chk______Invex").click()
    driver.find_element(By.ID, "chk______Santander").click()
    driver.find_element(By.ID, "chk______Scotiabank").click()
    driver.find_element(By.ID, "chk______Ve_por_Más").click()
    driver.find_element(By.ID, "chk_Accendo_Banco").click()
    driver.find_element(By.ID, "chk_American_Express").click()
    driver.find_element(By.ID, "chk_Mizuho_Bank").click()
    driver.find_element(By.ID, "chk_MUFG_Bank").click()
    driver.find_element(By.ID, "chk_Shinhan").click()
    
    driver.implicitly_wait(60)
    driver.find_element(By.ID, "btn_GeneraRoporte").click()

    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")

    driver.switch_to_frame(driver.find_element_by_id("IFrame_Container"))
    driver.switch_to_frame(driver.find_element_by_tag_name("iframe"))
    target = driver.find_element_by_xpath("//*[contains(@id, 'VisibleReportContentReportViewer')]").get_attribute('innerHTML')

    driver.quit()

    soup = BeautifulSoup(target, "html.parser")

    with open('./scrape040.csv', 'w', newline='') as f_output:
        csv_output = csv.writer(f_output, delimiter='|')

        for tr in soup.find_all('tr'):
            csv_output.writerow([td.text for td in tr.find_all('td')])
    return("Éxito!! archivo scrape040.csv generado!")    

@app.route('/cnvb/068', methods=['GET'])
def scrape068():
    driver = webdriver.Remote(command_executor='http://selenium.ds:4444/wd/hub',desired_capabilities=DesiredCapabilities.CHROME)
    driver.implicitly_wait(15)
    driver.get("https://picomponentebi.cnbv.gob.mx/ReportViwer/ReportService?sector=68&tema=2&subTema=3&tipoInformacion=0&subTipoInformacion=0&idReporte=068_11q_BIPER0&idPortafolio=0&idTipoReporteBI=1")
    driver.set_window_size(960, 1043)

    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")
        
    driver.implicitly_wait(2)
    driver.find_element(By.ID, "btn_OcultaFiltros").click()

    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")
        
    driver.find_element(By.CSS_SELECTOR, ".radio:nth-child(1) > div:nth-child(4) > label").click()
    driver.find_element(By.ID, "chk_202007").click()
    driver.find_element(By.ID, "chk_202006").click()
    driver.find_element(By.ID, "chk_202005").click()
    driver.find_element(By.ID, "chk_202004").click()
    driver.find_element(By.ID, "Institucion_6_H").click()

    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")

    driver.implicitly_wait(1)

    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(2) > td:nth-child(1) > label").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(3) > td:nth-child(1) > label").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(4) > td:nth-child(1) > label").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(5) > td:nth-child(1) > label").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(6) > td:nth-child(1) > label").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(7) > td:nth-child(1) > label").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(8) > td:nth-child(1) > label").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(1) > td:nth-child(2) > label").click()
    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")
    driver.find_element(By.ID, "chk_Finactiv").click()
    driver.find_element(By.ID, "chk_Financiera_Bajío").click()
    driver.find_element(By.ID, "chk_Financiera_Banregio").click()
    driver.find_element(By.ID, "chk_Finanmadrid").click()
    driver.find_element(By.ID, "chk_Ford_Credit").click()
    driver.find_element(By.ID, "chk_GM_Financial").click()
    driver.find_element(By.ID, "chk_ION_Financiera").click()
    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")
    element = driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D > .checkbox")
    actions = ActionChains(driver)
    actions.move_to_element(element).click_and_hold().perform()
    element = driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D > .checkbox")
    actions = ActionChains(driver)
    actions.move_to_element(element).perform()
    element = driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D > .checkbox")
    actions = ActionChains(driver)
    actions.move_to_element(element).release().perform()
    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(1) > td:nth-child(3) > label").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(2) > td:nth-child(3) > label").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(3) > td:nth-child(3) > label").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(4) > td:nth-child(3) > label").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(5) > td:nth-child(3) > label").click()
    driver.find_element(By.ID, "chk_Santander_Consumo").click()
    driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D tr:nth-child(7) > td:nth-child(3) > label").click()
    driver.find_element(By.ID, "chk_Santander_Vivienda").click()
    driver.find_element(By.ID, "chk_Servicios_Mega").click()
    element = driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D > .checkbox")
    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")
    actions = ActionChains(driver)
    actions.move_to_element(element).click_and_hold().perform()
    element = driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D > .checkbox")
    actions = ActionChains(driver)
    actions.move_to_element(element).perform()
    element = driver.find_element(By.CSS_SELECTOR, "#Institucion_6_D > .checkbox")
    actions = ActionChains(driver)
    actions.move_to_element(element).release().perform()
    driver.find_element(By.ID, "chk_Start_Banregio__antes:_AF_Banregio_").click()
    driver.find_element(By.ID, "chk_Value_Arrendadora").click()
    driver.find_element(By.ID, "Escala_3_H").click()

    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")

    driver.implicitly_wait(60)
    driver.find_element(By.ID, "btn_GeneraRoporte").click()

    try:
        driver.find_element(By.ID, "nonexistantElement")
    except:
        print("")

    driver.switch_to_frame(driver.find_element_by_id("IFrame_Container"))
    driver.switch_to_frame(driver.find_element_by_tag_name("iframe"))
    target = driver.find_element_by_xpath("//*[contains(@id, 'VisibleReportContentReportViewer')]").get_attribute('innerHTML')

    driver.quit()

    soup = BeautifulSoup(target, "html.parser")

    with open('./scrape068.csv', 'w', newline='') as f_output:
        csv_output = csv.writer(f_output, delimiter='|')

        for tr in soup.find_all('tr'):
            csv_output.writerow([td.text for td in tr.find_all('td')])
    return("Éxito!! archivo scrape068.csv generado!")
 
